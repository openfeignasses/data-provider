#!/bin/bash

set -e

# Start in watching mode for production
if [ -n "$KEEP_WATCHING_STOCK_QUOTATIONS" ]; then
    echo "Starting with keep watching stock_quotations table mode ..."
    while [ true ]
    do
        java -cp /app/target/data-provider-1.0-SNAPSHOT-jar-with-dependencies.jar ShouldRun && \
        java -jar /app/target/data-provider-1.0-SNAPSHOT-jar-with-dependencies.jar
        sleep 120 # loop every 2 minutes
    done
fi

# By default start with what is define in CMD
exec "$@"
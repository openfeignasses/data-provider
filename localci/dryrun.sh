if [ ! -d "../database-migrator" ]; then
    echo "Directory ../database-migrator doesn't exist. Exiting ..."
    exit 1
fi
echo "Recompiling code ..."
docker build -t data-provider .
cd ../database-migrator
docker build -t database-migrator . || exit 1
cd ../data-provider
echo "Executing ..."
running_db_ip=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' mysql)
docker run \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator
docker run \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator npm run clear-db
docker run \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator npm run fixtures:data-provider:dryrun
docker run \
    --rm \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e MYSQL_USER=admin \
    -e MYSQL_PASSWORD=frNH95eSiLYY \
    -e MYSQL_DATABASE=cryptobot \
    -e MYSQL_USE_SSL=false \
    data-provider
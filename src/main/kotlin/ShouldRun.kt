import kraken.KrakenOHLCsPersistable
import org.joda.time.DateTime
import utils.PersistenceConfigOverride
import kotlin.system.exitProcess

object ShouldRun {
    @JvmStatic
    fun main(args: Array<String>) {
        PersistenceConfigOverride.setDatabaseConnetion(
                System.getenv("DATABASE_HOST"),
                System.getenv("DATABASE_PORT"),
                System.getenv("MYSQL_DATABASE"),
                System.getenv("MYSQL_USER"),
                System.getenv("MYSQL_PASSWORD"),
                "false"
        )

        val maxAddDate = KrakenOHLCsPersistable.findMaxAddDate()

        if (maxAddDate == null) {
            println(DateTime.now().toString() + " | Should the program run ? Answer: yes")
            exitProcess(0)
        }
        else {
            val maxAddDate = maxAddDate as Int
            val fourHoursInSeconds = 4*3600
            val currentTimestampSeconds = DateTime.now().millis / 1000
            if (currentTimestampSeconds > maxAddDate.toLong() + fourHoursInSeconds.toLong()) {
                println(DateTime.now().toString() + " | Should the program run ? Answer: yes")
                exitProcess(0)
            }
        }
        println(DateTime.now().toString() + " | Should the program run ? Answer: no")
        exitProcess(1)
    }
}
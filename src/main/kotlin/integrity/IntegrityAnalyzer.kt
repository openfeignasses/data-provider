package integrity
import org.knowm.xchange.kraken.dto.marketdata.KrakenOHLCs

class IntegrityAnalyzer(ohlcs: KrakenOHLCs, currentTimestamp: Long, intervalInMins: Int) {

    val gotFatalError: Boolean = false
    var indexesToDelete = arrayListOf<Int>()
    var indexesToInterpolate = arrayListOf<Int>()

    init {
        var indexOfDuplicatedValues = detectDuplicates(ohlcs)
        indexesToDelete = adjustIndexesForDuplicated(indexOfDuplicatedValues)

        var indexesOfMissingValues = detectMissingValues(ohlcs, intervalInMins)
        errorIfLastValueMissing(ohlcs.ohlCs.last().time, currentTimestamp, intervalInMins)
        indexesToInterpolate = adjustIndexesForMissing(indexesOfMissingValues)
    }

    private fun errorIfLastValueMissing(lastOhlcTime: Long, currentTimestamp: Long, intervalInMins: Int) {
        val intervalSeconds = intervalInMins * 60
        val missingLastValueThreshold = currentTimestamp - (intervalSeconds + 1)
        println("++++++++++++++ lastOhlcTime:"+lastOhlcTime)
        println("++++++++++++++ missingLastValueThreshold:"+missingLastValueThreshold)
        if(lastOhlcTime < missingLastValueThreshold) {
            throw Exception("The time of the last OHLC is older than the defined threshold.")
        }
    }

    private fun adjustIndexesForDuplicated(indexOfDuplicatedValues: ArrayList<Int>): java.util.ArrayList<Int> {
        println("1: indexOfDuplicatedValues"+indexOfDuplicatedValues)

        // delete one of 2
        var indexOfDuplicatedValuesFiltered = indexOfDuplicatedValues.filterIndexed { index, _ -> index % 2 == 0 }

        println("2:indexOfDuplicatedValues"+indexOfDuplicatedValuesFiltered)

        // ensure uniqueness
        var indexOfDuplicatedValuesUnique: ArrayList<Int> = arrayListOf()
        indexOfDuplicatedValuesFiltered.forEach {
            if(!indexOfDuplicatedValuesUnique.contains(it)) {
                indexOfDuplicatedValuesUnique.add(it)
            }
        }
        println("3:indexOfDuplicatedValuesUnique"+indexOfDuplicatedValuesUnique)

        // shift
        var offset = 0
        for (i in 0..indexOfDuplicatedValuesUnique.size-1) {
            indexOfDuplicatedValuesUnique[i] -= offset++
        }

        println("4:indexOfDuplicatedValuesUnique"+indexOfDuplicatedValuesUnique)

        return indexOfDuplicatedValuesUnique
    }

    private fun detectDuplicates(ohlcs: KrakenOHLCs): ArrayList<Int> {
        var indexOfDuplicatedValues: ArrayList<Int> = arrayListOf()
        var index = 0
        while(index <= ohlcs.ohlCs.size-2) { // -2 because we cannot check for the last OHLC
            val currentOHLCTime = ohlcs.ohlCs[index].time
            val currentOHLCValue = ohlcs.ohlCs[index].close
            val nextOHLCTime = ohlcs.ohlCs[index+1].time
            val nextOHLCValue = ohlcs.ohlCs[index+1].close
            if (currentOHLCTime == nextOHLCTime && currentOHLCValue != nextOHLCValue) {
                throw Exception("Detected duplicated date with different values.")
            }
            else if (currentOHLCTime == nextOHLCTime && currentOHLCValue == nextOHLCValue) {
                println("]]]]]]]]]]]]]] DETECTED DUPLICATED DATE, WITH SAME VALUE [[[[[[[[[[[[[")
                indexOfDuplicatedValues.add(index)
                indexOfDuplicatedValues.add(index+1)
            }
            index++
        }
        return indexOfDuplicatedValues
    }

    private fun detectMissingValues(ohlcs: KrakenOHLCs, intervalInMins: Int): ArrayList<Int> {
        var indexesOfMissingValues: ArrayList<Int> = arrayListOf()

        for(index in 0..ohlcs.ohlCs.size-2) { // -2 because we cannot check for the last OHLC

            val currentOHLCTime = ohlcs.ohlCs[index].time
            val nextOHLCTime = ohlcs.ohlCs[index+1].time
            val deltaBetweenCurrentAndNext = getDelta(currentOHLCTime, nextOHLCTime)
            val expectedIntervalInSeconds: Int = intervalInMins * 60

            if (twoOrMoreMissingValues(deltaBetweenCurrentAndNext, expectedIntervalInSeconds)) {
                throw Exception("Two consecutive OHLC values are missing")
            }
            else if (oneMissingValue(deltaBetweenCurrentAndNext, expectedIntervalInSeconds)) {
                indexesOfMissingValues.add(index)
            }
        }
        return indexesOfMissingValues
    }

    private fun oneMissingValue(deltaBetweenCurrentAndNext: Long, expectedIntervalInSeconds: Int) =
            deltaBetweenCurrentAndNext > expectedIntervalInSeconds

    private fun twoOrMoreMissingValues(deltaBetweenCurrentAndNext: Long, expectedIntervalInSeconds: Int) =
            deltaBetweenCurrentAndNext > 2 * expectedIntervalInSeconds

    /**
     * Each time we add an entry to a list, indexes are shifted.
     * After adding the first value, the index of the second value should be increased by 1
     * After adding the second value, the index of the third value should be increased by 2
     * ...
     */
    private fun adjustIndexesForMissing(indexesOfMissingValues: ArrayList<Int>): java.util.ArrayList<Int> {
        var offset = 0
        for (i in 0..indexesOfMissingValues.size-1) {
            indexesOfMissingValues[i] += offset++
        }
        return indexesOfMissingValues
    }

    private fun getDelta(current: Long, next: Long): Long {
        return next - current
    }
}
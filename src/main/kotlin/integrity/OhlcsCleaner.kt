package integrity

import org.knowm.xchange.kraken.dto.marketdata.KrakenOHLC
import org.knowm.xchange.kraken.dto.marketdata.KrakenOHLCs
import utils.WarningCreator

class OHLCsCleaner(ohlCs: KrakenOHLCs, warningCreator: WarningCreator, currentTimestamp: Long, intervalInMins: Int) {

    private var fatalError: Boolean
    private var indexesOfDuplicatedValues: ArrayList<Int>
    private var indexesOfMissingValues: ArrayList<Int>
    private val ohlCs = ohlCs
    private var warningCreator = warningCreator
    var cleanedOHLCs: KrakenOHLCs

    init {
        val integrityAnalyzer = IntegrityAnalyzer(ohlCs, currentTimestamp, intervalInMins)
        fatalError = integrityAnalyzer.gotFatalError
        indexesOfDuplicatedValues = integrityAnalyzer.indexesToDelete
        indexesOfMissingValues = integrityAnalyzer.indexesToInterpolate

        cleanedOHLCs = deleteOHLCWhereNeeded()
        cleanedOHLCs = fillTheGaps(intervalInMins)
    }

    private fun deleteOHLCWhereNeeded(): KrakenOHLCs {
        var index = 0
        while (index <= indexesOfDuplicatedValues.size-1) {
            ohlCs.ohlCs.removeAt(indexesOfDuplicatedValues[index++])
            warningCreator.writeMessage("OHLC removed at index $index")
        }
        return ohlCs
    }

    private fun fillTheGaps(intervalInMins: Int): KrakenOHLCs {

        indexesOfMissingValues.forEach() {

            val currentOhlc = ohlCs.ohlCs[it]
            val nextOhlc = ohlCs.ohlCs[it + 1]
            val interpolatedValue = (currentOhlc.close + nextOhlc.close) / (2).toBigDecimal()
            val timestampOfInterpolation: Long = currentOhlc.time + 60 * intervalInMins
            warningCreator.writeMessage("OHLC interpolation created at date $timestampOfInterpolation")

            ohlCs.ohlCs.add(it+1, KrakenOHLC(
                    timestampOfInterpolation,
                    (-1).toBigDecimal(),
                    (-1).toBigDecimal(),
                    (-1).toBigDecimal(),
                    interpolatedValue,
                    (-1).toBigDecimal(),
                    (-1).toBigDecimal(),
                    -1
            )
            )
        }
        return ohlCs
    }
}
package kraken

import integrity.OHLCsCleaner
import model.InvestmentUniverseStatusRetriever
import model.InvestmentUniverseStatusUpdater
import model.LatestOhlcRetriever
import org.knowm.xchange.ExchangeFactory
import org.knowm.xchange.ExchangeSpecification
import org.knowm.xchange.currency.CurrencyPair
import org.knowm.xchange.kraken.KrakenExchange
import org.knowm.xchange.kraken.dto.marketdata.KrakenOHLCs
import org.knowm.xchange.kraken.service.KrakenMarketDataServiceRaw
import utils.WarningCreator

class KrakenConnector(var warningCreator: WarningCreator) {

    private lateinit var krakenExchange: KrakenExchange
    var httpConnTimeout: Int = 10000
    var httpReadTimeout: Int = 10000

    fun setUp() {
        val specification = ExchangeSpecification(KrakenExchange::class.java.name)
        if (System.getenv("MOCK_KRAKEN_API") == "true") {
            specification.host = "localhost"
            specification.port = 8080
            specification.sslUri = "http://localhost:8080"
        }
        specification.httpConnTimeout = httpConnTimeout
        specification.httpReadTimeout = httpReadTimeout

        krakenExchange = ExchangeFactory.INSTANCE.createExchange(specification) as KrakenExchange
    }

    fun getAndSaveStockQuotations(intervalInMins: Int, currentTimestamp: Long) {
        InvestmentUniverseStatusRetriever().findAll().forEach {

            val latestOhlc = LatestOhlcRetriever().findByPair(it.name)

            // get ohlcs since timeOfLatestOhlc

            // if number of ohlc results == 1
            // check if the only ohlc return is not yet saved
            // if number of ohlc results > 1, save everything

            getAndSaveStockQuotationsForCurrency(CurrencyPair(it.name.replace("_", "/")),
                    intervalInMins, latestOhlc, currentTimestamp)
        }
    }

    private fun getAndSaveStockQuotationsForCurrency(currencyPair: CurrencyPair, intervalInMins: Int, latestOhlc: KrakenOHLCPersistable?,
                                                     currentTimestamp: Long) {
        require(intervalInMins != null && intervalInMins > 0) { "Interval value is not valid." }

        try {

            val veryBeginning: Long = 1
            var since: Long = latestOhlc?.time ?: veryBeginning


            var ohlcs = getStockQuotations(currencyPair, intervalInMins, since)
            if(ohlcs.ohlCs.size == 1 && ohlcs.ohlCs.first().time == since) {
                return
            }


            ohlcs = OHLCsCleaner(ohlcs, warningCreator, currentTimestamp, intervalInMins).cleanedOHLCs

            saveLastPrice(currencyPair.toString().replace("/", "_"), ohlcs)

            updateInvestmentUniverseStatus(currencyPair.toString().replace("/", "_"), "TRADABLE")

        } catch (e: Exception) {
            updateInvestmentUniverseStatus(currencyPair.toString().replace("/", "_"), "NON_TRADABLE")
            e.printStackTrace()
            warningCreator.writeMessage("Failed to get and save quotations for $currencyPair")
        }

    }

    private fun updateInvestmentUniverseStatus(pairName: String, newStatus: String) {
        InvestmentUniverseStatusUpdater(pairName, newStatus)
    }

    private fun getStockQuotations(currency_pair: CurrencyPair?, interval: Int, since: Long): KrakenOHLCs {
        require(currency_pair != null) { "currency_pair parameter cannot be null." }

        val marketDataServiceRaw = krakenExchange.marketDataService as KrakenMarketDataServiceRaw
        return marketDataServiceRaw.getKrakenOHLC(currency_pair, interval, since)
    }

    private fun saveLastPrice(currency_pair: String, krakenOhlcs: KrakenOHLCs) {
        KrakenOHLCsPersistable(currency_pair, krakenOhlcs).persist()
    }
}

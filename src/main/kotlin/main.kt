import kraken.KrakenConnector
import kraken.KrakenOHLCsPersistable
import org.joda.time.DateTime
import utils.PersistenceConfigOverride
import utils.WarningCreator

fun main() {
    PersistenceConfigOverride.setDatabaseConnetion(
            System.getenv("DATABASE_HOST"),
            System.getenv("DATABASE_PORT"),
            System.getenv("MYSQL_DATABASE"),
            System.getenv("MYSQL_USER"),
            System.getenv("MYSQL_PASSWORD"),
            System.getenv("MYSQL_USE_SSL") ?: "true" 
    )

    val krakenConnector = KrakenConnector(WarningCreator())
    krakenConnector.setUp()

    val FREQUENCY_IN_MINS = 60
    val currentTimestamp = DateTime.now().millis / 1000
    krakenConnector.getAndSaveStockQuotations(FREQUENCY_IN_MINS, currentTimestamp)
}
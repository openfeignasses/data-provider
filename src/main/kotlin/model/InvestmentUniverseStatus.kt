package model

import utils.NoArg
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

/**
 * Entity that enables persisting investment universe status.
 */
@Entity @NoArg
@Table(name="investment_universe_status")
data class InvestmentUniverseStatus(
    @Id
    @GeneratedValue
    val id: Int = 0,
    val name: String = "",
    var status: String = ""
)
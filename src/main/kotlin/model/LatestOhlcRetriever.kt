package model

import kraken.KrakenOHLCPersistable
import org.knowm.xchange.kraken.dto.marketdata.KrakenOHLC
import utils.PersistenceConfigOverride
import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence
import javax.persistence.Query

class LatestOhlcRetriever() {

    private lateinit var investmentUniverseStatusList: List<InvestmentUniverseStatus>
    lateinit var investmentUniverseStatus: InvestmentUniverseStatus
    var entityManager: EntityManager? = null

    fun findByPair(pair: String): KrakenOHLCPersistable? {
        val emFactory: EntityManagerFactory = Persistence.createEntityManagerFactory("PersistenceProviderMysql", PersistenceConfigOverride.configMap)
        entityManager = emFactory.createEntityManager()

        val qlQuery = "SELECT k FROM kraken.KrakenOHLCPersistable k " +
                "WHERE k.currency_pair = :currency_pair " +
                "ORDER BY k.time DESC"
        val query: Query? = entityManager?.createQuery(qlQuery)?.setParameter("currency_pair", pair)
        try {
            query?.maxResults = 1
            val latestOhlc = query?.singleResult as KrakenOHLCPersistable
            println("latestOhlcTime:"+latestOhlc)
            return latestOhlc
        } catch (e: javax.persistence.NoResultException) {
            return null
        } finally {
            entityManager?.close()
            emFactory.close()
        }
    }
}
package utils

import com.github.salomonbrys.kotson.jsonArray
import com.github.salomonbrys.kotson.jsonObject
import com.github.salomonbrys.kotson.set
import com.google.gson.JsonObject

class KrakenApiResultsErrorGenerator () {

    var jsonObject: JsonObject = jsonObject(
        "error" to jsonArray(null)
    )

    fun setFirstError(errorString: String) {
        jsonObject.getAsJsonArray("error")[0] = errorString
    }
}

package utils

import com.github.salomonbrys.kotson.jsonArray
import com.github.salomonbrys.kotson.jsonObject
import com.google.gson.JsonObject
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.ZoneOffset
import kotlin.collections.ArrayList

class KrakenApiResultsGenerator (pairName: String, firstDate: LocalDateTime, intervalInHours: Int) {

    val pairName: String = pairName
    val firstDate: LocalDateTime = firstDate
    val intervalInHours: Int = intervalInHours
    var since: Long? = null
    var jsonObject: JsonObject = jsonObject(
        "error" to jsonArray(),
        "result" to jsonObject(
            pairName to jsonArray(),
            "last" to null
        )
    )

    init {
        calculateSince()
        println("calculateSince:$since")
        println("since?.toInt(): ${since?.toInt()}")
    }

    private fun calculateSince() {
        since = firstDate.toEpochSecond(ZoneOffset.UTC)
    }

    private fun addQuotation(quotation: Map<String, Any?>) {
        jsonObject.getAsJsonObject("result").getAsJsonArray(pairName).add(jsonArray(
                quotation.get("time"),
                quotation.get("open"),
                quotation.get("high"),
                quotation.get("low"),
                quotation.get("close"),
                quotation.get("vwap"),
                quotation.get("volume"),
                quotation.get("count")
        ))
    }

    private fun addQuotations(quotations: ArrayList<Map<String, Any?>>) {
        for(quotation in quotations) {
            addQuotation(quotation)
        }
    }

    fun createQuotation(time: Int?, close: BigDecimal?): Map<String, Any?> {
        return mapOf(
                "time" to time,
                "open" to 0,
                "high" to 0,
                "low" to 0,
                "close" to close,
                "vwap" to 0,
                "volume" to 0,
                "count" to 0
        )
    }

    fun addQuotationsFromValuesList(values: List<Int>) {
        var quotations: ArrayList<Map<String, Any?>> = ArrayList()
        for((index, value) in values.withIndex()) {
            if(value != -1) {
                quotations.add(createQuotation(since?.toInt(), value.toBigDecimal()))
            }
            incrementSince()
        }
        addQuotations(quotations)
    }

    fun addQuotationsFromValuesListWithDuplicates(values: List<List<Int>>) {
        var quotations: ArrayList<Map<String, Any?>> = ArrayList()
        for((index, value) in values.withIndex()) {
            if(value[0] != -1) {
                value.forEach {
                    quotations.add(createQuotation(since?.toInt(), it.toBigDecimal()))
                }
            }
            incrementSince()
        }
        addQuotations(quotations)
    }

    private fun incrementSince() {
        since = since?.plus(intervalInHours * 3600)
    }

    fun addQuotationsFromTable(lines: List<List<String?>>) {
        var quotations: ArrayList<Map<String, Any?>> = ArrayList()
        for((index, value) in lines.withIndex()) {
            if (value.get(1) != "null") {
                quotations.add(createQuotation(value.get(0)?.toInt(), value.get(1)?.toBigDecimal()))
            }
            else {
                quotations.add(createQuotation(value.get(0)?.toInt(), null))
            }
        }
        addQuotations(quotations)
    }

}

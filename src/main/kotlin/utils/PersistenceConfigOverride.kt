package utils

class PersistenceConfigOverride {
    companion object {
        var configMap: Map<String, String>? = null

        fun setDatabaseConnetion(host: String,
                                 port:String,
                                 schema: String,
                                 user: String,
                                 password: String,
                                 useSSL: String) {
            configMap = mapOf(
                    "javax.persistence.jdbc.url" to "jdbc:mysql://$host:$port/$schema?useSSL=$useSSL&allowPublicKeyRetrieval=true&rewriteBatchedStatements=true",
                    "javax.persistence.jdbc.user" to "$user",
                    "javax.persistence.jdbc.password" to "$password"
            )
        }
    }
}
package utils

import org.joda.time.DateTime
import java.io.File

class WarningCreator {

    fun writeMessage(message: String) {

        var warningFile: File

        val name: String = "warning."+ DateTime.now().millis + ".txt"
        warningFile = File("warnings/"+name)
        val fileCreated :Boolean = warningFile.createNewFile()

        if(fileCreated){
            println("Warning file created successfully.")
        } else{
            println("Warning file already exists.")
        }

        warningFile.writeText(message+"\n")
    }
}

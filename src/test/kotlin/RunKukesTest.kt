import cucumber.api.CucumberOptions
import org.junit.runner.RunWith

@RunWith(CucumberCustom::class)
@CucumberOptions(
        features = ["src/test/resources/cucumber/features"],
        tags = ["not @WIP"],
        plugin = ["pretty", "html:target/cucumber"])
class RunKukesTest


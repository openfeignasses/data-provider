import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import cucumber.api.Scenario
import cucumber.api.java8.En
import kraken.KrakenConnector
import kraken.KrakenOHLCsPersistable
import model.InvestmentUniverseStatusRetriever
import model.InvestmentUniverseStatusUpdater
import org.apache.commons.io.FileUtils
import java.io.File


class StepDefsHooks(var world: World, val krakenConnector: KrakenConnector): En {

    init {

        Before(arrayOf("@Stub")) { scenario: Scenario ->
            world.wireMockServer = WireMockServer()
            world.wireMockServer.start()
            world.stubResponseFor4HoursFreqency = aResponse()
            world.stubResponseFor1HourFreqency = aResponse()
            world.stubResponses = mapOf()

            krakenConnector.setUp()
            KrakenOHLCsPersistable.truncate()
        }

        After(arrayOf("@Stub")) { scenario: Scenario ->
            world.wireMockServer.stop()
        }

        // AfterEach
        After(arrayOf("@StockQuotationsProvider")) { scenario: Scenario ->
            // Reset statuses
            InvestmentUniverseStatusRetriever().findAll().forEach {
                setStatusToForceIgnore(it.name)
            }
            // Reset warnings
            FileUtils.deleteDirectory(File("warnings"))
            File("warnings").mkdirs();
        }
    }

    private fun setStatusToForceIgnore(pairName: String) {
        InvestmentUniverseStatusUpdater(pairName, "FORCE_IGNORE")
    }
}
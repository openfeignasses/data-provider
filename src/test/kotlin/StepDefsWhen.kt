import cucumber.api.java8.En
import kraken.KrakenConnector
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter


class StepDefsWhen(var world: World, val krakenConnector: KrakenConnector): En {

    init {

        When("I (try to |)get stock quotations from the platform( without error|)") {
            try {
                var currentTimestamp: Long = DateTime.parse(world.currentDate,
                        DateTimeFormat.forPattern("dd/MM/yyyy HH:mm")).millis / 1000

                krakenConnector.getAndSaveStockQuotations(world.frequency*60, currentTimestamp)
            } catch (e: Exception) {
                e.printStackTrace()
                world.errorOccured = true
            }
        }
    }
}
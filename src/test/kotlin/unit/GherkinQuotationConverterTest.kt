package unit

import utils.GherkinQuotationConverter
import com.github.jasync.sql.db.util.size
import org.junit.Test

class GherkinQuotationConverterTest {

    private val tableConverter = GherkinQuotationConverter(" \n ")

    @Test
    fun testSplitString() {
        assert(tableConverter.splitString().size == 2)
    }

    @Test
    fun testIgnoreFirstAndLastLines() {
        var list:List<String> = listOf(" XX ", " XX ", " XX ")
        assert(tableConverter.ignoreFirstAndLastLines(list).size == 1)
    }

    @Test
    fun keepOnlyBetweenFirstAndLastColumn() {
        val tableConverterSecondary = GherkinQuotationConverter(" XX ")
        tableConverterSecondary.splitString()
        var list:List<String> = listOf(" XX ", " XX ", " XX ")
        assert(tableConverterSecondary.keepOnlyBetweenFirstAndLastColumn(list).all {
            it.size == 2
        })
    }

    @Test
    fun testConvertLinesToColumns() {
        var list:List<String> = listOf("X  ", " XX")
        assert(tableConverter.convertLinesToColumns(list).equals(arrayListOf(" X", "X ", "X ")))
    }

    @Test
    fun testTranslateXToValues1() {
        val arrayList:ArrayList<String> = arrayListOf(" X", "X ", "X ")
        assert(tableConverter.translateXToValues(arrayList).equals(listOf(1, 0, 0)))
    }

    @Test
    fun testTranslateXToValues2() {
        val arrayList:ArrayList<String> = arrayListOf(" X", "X ", "X ")
        assert(tableConverter.translateXToValuesWithDuplicates(arrayList).equals(listOf(listOf(1), listOf(0), listOf(0))))
    }

    @Test
    fun testTranslateXToValues3() {
        val arrayList:ArrayList<String> = arrayListOf(" X", "XX", "X ")
        assert(tableConverter.translateXToValuesWithDuplicates(arrayList).equals(listOf(listOf(1), listOf(0, 1), listOf(0))))
    }
}

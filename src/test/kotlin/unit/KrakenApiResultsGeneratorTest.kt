package unit

import com.github.salomonbrys.kotson.get
import com.google.gson.JsonNull
import com.google.gson.JsonObject
import org.junit.Test
import org.powermock.reflect.Whitebox
import utils.KrakenApiResultsGenerator
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter


class KrakenApiResultsGeneratorTest() {

    var formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")
    private val krakenApiResultsGenerator = KrakenApiResultsGenerator("XXBTZEUR",
            LocalDateTime.parse("31/12/2019 00:00", formatter), 4)

    @Test
    fun testCreateResultStructure() {
        assert(krakenApiResultsGenerator.jsonObject is JsonObject)
        assert(krakenApiResultsGenerator.jsonObject.has("error"))
        assert(krakenApiResultsGenerator.jsonObject.has("result"))
        assert(krakenApiResultsGenerator.jsonObject.getAsJsonObject("result").has("last"))
    }

    @Test
    fun testPresencePairName() {
        assert(krakenApiResultsGenerator.jsonObject.getAsJsonObject("result").has("XXBTZEUR"))
    }

    @Test
    fun testAddOneQuotation() {
        Whitebox.invokeMethod<Unit>(krakenApiResultsGenerator, "addQuotation", mapOf(
                "time" to 1,
                "open" to null,
                "high" to null,
                "low" to null,
                "close" to 10,
                "vwap" to null,
                "volume" to null,
                "count" to null
        ))
        val pairResult = krakenApiResultsGenerator.jsonObject.getAsJsonObject("result").getAsJsonArray("XXBTZEUR")
        println(pairResult)
        assert(pairResult.size() == 1)
        assert(pairResult.get(0)[0].asInt == 1)
        assert(pairResult.get(0)[4].asInt == 10)
    }

    @Test
    fun testCreateQuotationWithCloseAndTime() {
        val quotation: Map<String, Any?> = krakenApiResultsGenerator.createQuotation(1, 10.toBigDecimal())
        assert(quotation["time"]?.equals(1)!!)
        assert(quotation["close"]?.equals(10.toBigDecimal())!!)
    }

    @Test
    fun testAddQuotations() {
        Whitebox.invokeMethod<Unit>(krakenApiResultsGenerator, "addQuotations", arrayListOf(
            krakenApiResultsGenerator.createQuotation(1, 10.toBigDecimal()),
            krakenApiResultsGenerator.createQuotation(2, 20.toBigDecimal())
        ))
        val pairResult = krakenApiResultsGenerator.jsonObject.getAsJsonObject("result").getAsJsonArray("XXBTZEUR")
        assert(pairResult.size() == 2)
        assert(pairResult.get(0)[0].asInt == 1)
        assert(pairResult.get(0)[4].asInt == 10)
        assert(pairResult.get(1)[0].asInt == 2)
        assert(pairResult.get(1)[4].asInt == 20)
        println(krakenApiResultsGenerator.jsonObject)
    }

    @Test
    fun testAddQuotationsFromValuesList1() {
        krakenApiResultsGenerator.addQuotationsFromValuesList(listOf(
                10, 20
        ))
        val pairResult = krakenApiResultsGenerator.jsonObject.getAsJsonObject("result").getAsJsonArray("XXBTZEUR")
        println(krakenApiResultsGenerator.jsonObject)
        assert(pairResult.size() == 2)
        assert(pairResult.get(0)[0].asInt == 1577750400)
        assert(pairResult.get(0)[4].asInt == 10)
        assert(pairResult.get(1)[0].asInt == 1577750400 + 4 * 3600)
        assert(pairResult.get(1)[4].asInt == 20)
    }

    @Test
    fun testAddQuotationsFromValuesList2() {
        krakenApiResultsGenerator.addQuotationsFromValuesListWithDuplicates(listOf(
                listOf(10), listOf(20)
        ))
        val pairResult = krakenApiResultsGenerator.jsonObject.getAsJsonObject("result").getAsJsonArray("XXBTZEUR")
        println(krakenApiResultsGenerator.jsonObject)
        assert(pairResult.size() == 2)
        assert(pairResult.get(0)[0].asInt == 1577750400)
        assert(pairResult.get(0)[4].asInt == 10)
        assert(pairResult.get(1)[0].asInt == 1577750400 + 4 * 3600)
        assert(pairResult.get(1)[4].asInt == 20)
    }

    @Test
    fun testAddQuotationsFromValuesList3() {
        krakenApiResultsGenerator.addQuotationsFromValuesListWithDuplicates(listOf(
                listOf(10, 10), listOf(20)
        ))
        val pairResult = krakenApiResultsGenerator.jsonObject.getAsJsonObject("result").getAsJsonArray("XXBTZEUR")
        println(krakenApiResultsGenerator.jsonObject)
        assert(pairResult.size() == 3)
        assert(pairResult.get(0)[0].asInt == 1577750400)
        assert(pairResult.get(0)[4].asInt == 10)
        assert(pairResult.get(1)[0].asInt == 1577750400)
        assert(pairResult.get(1)[4].asInt == 10)
        assert(pairResult.get(2)[0].asInt == 1577750400 + 4 * 3600)
        assert(pairResult.get(2)[4].asInt == 20)
    }

    @Test
    fun testAddQuotationsFromValuesList4() {
        krakenApiResultsGenerator.addQuotationsFromValuesListWithDuplicates(listOf(
                listOf(10, 5), listOf(20)
        ))
        val pairResult = krakenApiResultsGenerator.jsonObject.getAsJsonObject("result").getAsJsonArray("XXBTZEUR")
        println(krakenApiResultsGenerator.jsonObject)
        assert(pairResult.size() == 3)
        assert(pairResult.get(0)[0].asInt == 1577750400)
        assert(pairResult.get(0)[4].asInt == 10)
        assert(pairResult.get(1)[0].asInt == 1577750400)
        assert(pairResult.get(1)[4].asInt == 5)
        assert(pairResult.get(2)[0].asInt == 1577750400 + 4 * 3600)
        assert(pairResult.get(2)[4].asInt == 20)
    }

    @Test
    fun testAddQuotationsFromValuesList5() {
        krakenApiResultsGenerator.addQuotationsFromValuesListWithDuplicates(listOf(
                listOf(10), listOf(20, 19)
        ))
        val pairResult = krakenApiResultsGenerator.jsonObject.getAsJsonObject("result").getAsJsonArray("XXBTZEUR")
        println(krakenApiResultsGenerator.jsonObject)
        assert(pairResult.size() == 3)
        assert(pairResult.get(0)[0].asInt == 1577750400)
        assert(pairResult.get(0)[4].asInt == 10)
        assert(pairResult.get(1)[0].asInt == 1577750400 + 4 * 3600)
        assert(pairResult.get(1)[4].asInt == 20)
        assert(pairResult.get(2)[0].asInt == 1577750400 + 4 * 3600)
        assert(pairResult.get(2)[4].asInt == 19)
    }

    @Test
    fun testAddQuotationsFromTable1() {
        val krakenApiResultsGeneratorSecondary = KrakenApiResultsGenerator(
                "XXBTZEUR",
                Instant.ofEpochSecond(1586531940).atZone(ZoneOffset.UTC).toLocalDateTime(),
                4)
        krakenApiResultsGeneratorSecondary.addQuotationsFromTable(listOf(
                listOf("1586531940", "6636.3")
        ))
        val pairResult = krakenApiResultsGeneratorSecondary.jsonObject.getAsJsonObject("result").getAsJsonArray("XXBTZEUR")
        println(krakenApiResultsGeneratorSecondary.jsonObject)
        assert(pairResult.size() == 1)
        assert(pairResult.get(0)[0].asInt == 1586531940)
        assert(pairResult.get(0)[4].asBigDecimal == (6636.3).toBigDecimal())
    }

    @Test
    fun testAddQuotationsFromTable2() {
        val krakenApiResultsGeneratorSecondary = KrakenApiResultsGenerator(
                "XXBTZEUR",
                Instant.ofEpochSecond(1586531940).atZone(ZoneOffset.UTC).toLocalDateTime(),
                4)
        krakenApiResultsGeneratorSecondary.addQuotationsFromTable(listOf(
                listOf("1586531940", "6636.3730810289")
        ))
        val pairResult = krakenApiResultsGeneratorSecondary.jsonObject.getAsJsonObject("result").getAsJsonArray("XXBTZEUR")
        println(krakenApiResultsGeneratorSecondary.jsonObject)
        assert(pairResult.size() == 1)
        assert(pairResult.get(0)[0].asInt == 1586531940)
        assert(pairResult.get(0)[4].asBigDecimal.toString() == (6636.3730810289).toBigDecimal().toString())
    }

    @Test
    fun testAddQuotationsFromTable3() {
        val krakenApiResultsGeneratorSecondary = KrakenApiResultsGenerator(
                "XXBTZEUR",
                Instant.ofEpochSecond(1586531940).atZone(ZoneOffset.UTC).toLocalDateTime(),
                4)
        krakenApiResultsGeneratorSecondary.addQuotationsFromTable(listOf(
                listOf("1586531940", "null")
        ))
        val pairResult = krakenApiResultsGeneratorSecondary.jsonObject.getAsJsonObject("result").getAsJsonArray("XXBTZEUR")
        println(krakenApiResultsGeneratorSecondary.jsonObject)
        assert(pairResult.size() == 1)
        assert(pairResult.get(0)[0].asInt == 1586531940)
        assert(pairResult.get(0)[4] == JsonNull.INSTANCE)
    }
}
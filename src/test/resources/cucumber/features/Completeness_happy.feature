@StockQuotationsProvider
@StockQuotationsProvider_Completeness_happy
@PlatformIndependent
@Stub
Feature: Validate the amount of entries we got for stock quotations

   As Bernard the Data provider
   In order to provide only valid data to the other modules
   I want an error to be thrown if the returned stock quotations do not respect the defined completeness rules.
   This should be done for all available currencies pairs.
   If a stock quotation is incomplete, the associated currency pair is marked as faulty and is not used
   for the next steps until the faulty flag disappears.

   Background:
      Given the "BTC_EUR" currency is marked as "TRADABLE"

   @StockQuotationsProvider_Completeness_happy_1
   Scenario: Complete
      Given the frequency is "4" hours
      And the period is "3" days
      And the first date is "01/01/2020 00:00"
      And the last date is "03/01/2020 20:00"
      And the current date is "03/01/2020 20:01"
      And the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------+
      |                  |
      |    X           XX|
      |   X X        XX  |
      |  X   X      X    |
      | X     XXXXXX     |
      |X                 |
      +------------------+
      """
      When I get stock quotations from the platform
      Then we obtain these values for "BTC_EUR"
      """
      +------------------+
      |                  |
      |    X           XX|
      |   X X        XX  |
      |  X   X      X    |
      | X     XXXXXX     |
      |X                 |
      +------------------+
      """
      And the number of returned values is 18
      And the first date of the returned values is "01/01/2020 00:00"
      And the last date of the returned values is "03/01/2020 20:00"

   @StockQuotationsProvider_Completeness_happy_2
   Scenario: Complete
      Given the frequency is "4" hours
      And the period is "1200" days
      And the first date is "05/09/2019 20:00"
      And the last date is "03/01/2020 20:00"
      And the current date is "03/01/2020 20:00"
      And the "BTC_EUR" stock quotation available on the platform has a max length of "720" values
      When I get stock quotations from the platform
      Then the number of returned values is 720
      And the first date of the returned values is "05/09/2019 20:00"
      And the last date of the returned values is "03/01/2020 16:00"

   @StockQuotationsProvider_Completeness_happy_3
   Scenario: Deletion of the faulty flag
      Given the frequency is "4" hours
      And the period is "3" days
      And the first date is "01/01/2020 00:00"
      And the last date is "03/01/2020 00:00"
      And the current date is "03/01/2020 20:00"
      And the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------+
      |                  |
      |    X           XX|
      |   X X        XX  |
      |  X   X      X    |
      | X     XXXXXX     |
      |X                 |
      +------------------+
      """
      And the "BTC_EUR" currency is marked as "NON_TRADABLE"
      When I get stock quotations from the platform without error
      Then the "BTC_EUR" currency is flagged as "TRADABLE"

   @StockQuotationsProvider_Completeness_happy_4
   @MultipleCurrencies
   Scenario: Complete with multiple currencies
      Given the frequency is "4" hours
      And the period is "3" days
      And the first date is "01/01/2020 00:00"
      And the last date is "03/01/2020 20:00"
      And the current date is "03/01/2020 20:00"
      And the "BTC_EUR" currency is marked as "TRADABLE"
      And the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------+
      |                  |
      |    X           XX|
      |   X X        XX  |
      |  X   X      X    |
      | X     XXXXXX     |
      |X                 |
      +------------------+
      """
      And the "ETH_EUR" currency is marked as "TRADABLE"
      And the "ETH_EUR" stock quotation available on the platform is
      """
      +------------------+
      |X                X|
      | X              X |
      |  X          XXX  |
      |   XXXX  XXXX     |
      |       XX         |
      |                  |
      +------------------+
      """
      When I get stock quotations from the platform
      Then we obtain these values for "BTC_EUR"
      """
      +------------------+
      |                  |
      |    X           XX|
      |   X X        XX  |
      |  X   X      X    |
      | X     XXXXXX     |
      |X                 |
      +------------------+
      """
      Then we obtain these values for "ETH_EUR"
      """
      +------------------+
      |X                X|
      | X              X |
      |  X          XXX  |
      |   XXXX  XXXX     |
      |       XX         |
      |                  |
      +------------------+
      """
      And the number of returned values is 18 for "BTC_EUR"
      And the number of returned values is 18 for "ETH_EUR"
@StockQuotationsProvider
@StockQuotationsProvider_Completeness_unhappy_combined
@PlatformIndependent
@Stub
Feature: Validate the amount of entries we got for stock quotations

   As Bernard the Data provider
   In order to provide only valid data to the other modules
   I want an error to be thrown if the returned stock quotations do not respect the defined completeness rules.
   This should be done for all available currencies pairs.
   If a stock quotation is incomplete, the associated currency pair is marked as faulty and is not used
   for the next steps until the faulty flag disappears.

   Background:
      Given the frequency is "4" hours
      And the period is "3" days
      And the first date is "01/01/2020 00:00"
      And the last date is "03/01/2020 20:00"
      And the current date is "03/01/2020 20:01"
      And the "BTC_EUR" currency is marked as "TRADABLE"

   @StockQuotationsProvider_Completeness_unhappy_combined_1
   Scenario: Incomplete
      Given the "BTC_EUR" stock quotation with duplicates available on the platform is
      """
      +------------------+
      |                X |
      |    X           X |
      |   X X        XX  |
      |  X   X      X    |
      | X     XXXXXX     |
      |X                 |
      +------------------+
                       ^^
                       ||
              Duplicated,missing
      """
      When I get stock quotations from the platform
      Then a warning file is created
      And the "BTC_EUR" currency is marked as "NON_TRADABLE"

   @StockQuotationsProvider_Completeness_unhappy_combined_2
   Scenario: Incomplete
      Given the "BTC_EUR" stock quotation with duplicates available on the platform is
      """
      +------------------+
      |    X             |
      |    X           XX|
      |   X X        XX  |
      |  X   X      X    |
      | X     XX XXX     |
      |X                 |
      +------------------+
           ^    ^
           |    |
       Duplicated,missing
      """
      When I get stock quotations from the platform
      Then a warning file is created
      And the "BTC_EUR" currency is marked as "NON_TRADABLE"
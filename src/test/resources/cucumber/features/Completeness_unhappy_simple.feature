@StockQuotationsProvider
@StockQuotationsProvider_Completeness_unhappy_simple
@PlatformIndependent
@Stub
Feature: Validate the amount of entries we got for stock quotations

   As Bernard the Data provider
   In order to provide only valid data to the other modules
   I want an error to be thrown if the returned stock quotations do not respect the defined completeness rules.
   This should be done for all available currencies pairs.

   If a stock quotation is incomplete, the associated currency pair is marked as faulty and is not used
   in other modules until the faulty flag disappears. If we successfully get the faulty stock quotation,
   the faulty flag disappears.

   We consider that a value is missing in results returned by the platform when the time difference
   between two values is greater than the frequency.
   Each time this occurs, we create a new value to fill the gap: we add the missing timestamp and associate it
   to the value -1. If there are missing values multiple times in a row, then only one -1 value is added.

   Background:
      Given the frequency is "4" hours
      And the period is "3" days
      And the first date is "31/12/2019 00:00"
      And the last date is "03/01/2020 00:00"
      And the current date is "03/01/2020 00:00"
      And the "BTC_EUR" currency is marked as "TRADABLE"

   @StockQuotationsProvider_Completeness_unhappy_simple_1
   Scenario: Missing value at the end
      Given the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------+
      |                  |
      |    X           X |
      |   X X        XX  |
      |  X   X      X    |
      | X     XXXXXX     |
      |X                 |
      +------------------+
                        ^
                        |
                     Missing
      """
      When I get stock quotations from the platform
      Then a warning file is created
      And there is "0" -1 value in the result
      And the "BTC_EUR" currency is marked as "NON_TRADABLE"

   @StockQuotationsProvider_Completeness_unhappy_simple_2
   Scenario: Missing values at the end
      Given the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------+
      |                  |
      |    X             |
      |   X X        X   |
      |  X   X      X    |
      | X     XXXXXX     |
      |X                 |
      +------------------+
                      ^^^
                      |||
                     Missing
      """
      When I get stock quotations from the platform
      And there is "0" -1 value in the result
      And the "BTC_EUR" currency is marked as "NON_TRADABLE"
      
   @StockQuotationsProvider_Completeness_unhappy_simple_3
   Scenario: Incomplete somewhere in the middle
      Given the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------+
      |                  |
      |    X           XX|
      |   X X        XX  |
      |  X   X      X    |
      | X     XX XXX     |
      |X                 |
      +------------------+
                ^
                |
             Missing
      """
      When I get stock quotations from the platform
      Then the number of returned values is 18
      And we obtain these values for "BTC_EUR"
      """
      +------------------+
      |                  |
      |    X           XX|
      |   X X        XX  |
      |  X   X      X    |
      | X     XXXXXX     |
      |X                 |
      +------------------+
      """
      
   @StockQuotationsProvider_Completeness_unhappy_simple_4
   Scenario: Missing value at the beginning
      Given the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------+
      |                  |
      |    X           XX|
      |   X X        XX  |
      |  X   X      X    |
      | X     XXXXXX     |
      |                  |
      +------------------+
       ^
       |
    Missing
      """
      When I get stock quotations from the platform
      Then there is "0" -1 value in the result
      And the number of returned values is 17
      And we obtain these values for "BTC_EUR"
      """
      +-----------------+
      |                 |
      |   X           XX|
      |  X X        XX  |
      | X   X      X    |
      |X     XXXXXX     |
      |                 |
      +-----------------+
      """

   @StockQuotationsProvider_Completeness_unhappy_simple_5
   Scenario: Missing at two different places: beginning and middle
      Given the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------+
      |                  |
      |    X           XX|
      |   X X        XX  |
      |  X   X      X    |
      | X     XX XXX     |
      |                  |
      +------------------+
       ^        ^
       |        |
         Missing
      """
      When I get stock quotations from the platform
      Then there are "0" -1 values in the result
      And the number of returned values is 17
      And we obtain these values for "BTC_EUR"
      """
      +-----------------+
      |                 |
      |   X           XX|
      |  X X        XX  |
      | X   X      X    |
      |X     XXXXXX     |
      |                 |
      +-----------------+
      """

   @StockQuotationsProvider_Completeness_unhappy_simple_6
   Scenario: Missing at two different places: beginning and middle
      Given the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------+
      |                  |
      |    X           XX|
      |   X X        XX  |
      |      X      X    |
      | X     XX XXX     |
      |X                 |
      +------------------+
         ^      ^
         |      |
          Missing
      """
      When I get stock quotations from the platform
      Then the number of returned values is 18
      And we obtain these values for "BTC_EUR"
      """
      +------------------+
      |                  |
      |    X           XX|
      |   X X        XX  |
      |  X   X      X    |
      | X     XXXXXX     |
      |X                 |
      +------------------+
      """

   @StockQuotationsProvider_Completeness_unhappy_simple_7
   Scenario: Missing two times in a row
      Given the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------+
      |                  |
      |    X           XX|
      |   X X        XX  |
      |  X   X      X    |
      | X     XX  XX     |
      |X                 |
      +------------------+
                ^^
                ||
             Missing
      """
      When I get stock quotations from the platform
      Then a warning file is created
      And the "BTC_EUR" currency is marked as "NON_TRADABLE"

   @StockQuotationsProvider_Completeness_unhappy_simple_8
   Scenario: Missing lots of times in a row
      Given the "BTC_EUR" stock quotation available on the platform is
      """
      +------------------+
      |                  |
      |    X           XX|
      |   X X         X  |
      |  X   X           |
      | X     XX         |
      |X                 |
      +------------------+
                ^^^^^^
                ||||||
                Missing
      """
      When I get stock quotations from the platform
      Then a warning file is created
      And the "BTC_EUR" currency is marked as "NON_TRADABLE"
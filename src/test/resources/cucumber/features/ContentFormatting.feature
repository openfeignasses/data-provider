@StockQuotationsProvider
@StockQuotationsProvider_Formatting
@PlatformIndependent
@Stub
Feature: Format the values obtained for the stock quotations

   As Bernard the Data provider
   In order to have the control on how accurate the stock quotations values are
   I want the values to be formatted with the defined rules
   after they have been imported in a data structure.
   This should be done for all available currencies pairs.

   Background:
      Given the frequency is "4" hours
      And the "BTC_EUR" currency is marked as "TRADABLE"
      And the current date is "10/04/2020 15:20"

   @StockQuotationsProvider_Formatting_1
   Scenario: Few digits
      Given the "BTC_EUR" stock quotation table available on the platform is
         | time         | close           |
         | 1586531940   | 6636.3          |
      When I get stock quotations from the platform
      Then we obtain this values table for "BTC_EUR"
         | time         | close           |
         | 1586531940   | 6636.3000000    |

   @StockQuotationsProvider_Formatting_2
   Scenario: Few digits
      Given the "BTC_EUR" stock quotation table available on the platform is
         | time         | close           |
         | 1586531940   | 6636.373        |
      When I get stock quotations from the platform
      Then we obtain this values table for "BTC_EUR"
         | time         | close           |
         | 1586531940   | 6636.37300000   |

   @StockQuotationsProvider_Formatting_3
   Scenario: Too many digits
      Given the "BTC_EUR" stock quotation table available on the platform is
         | time         | close           |
         | 1586531940   | 6636.3730810289 |
      When I get stock quotations from the platform
      Then we obtain this values table for "BTC_EUR"
         | time         | close           |
         | 1586531940   | 6636.37308103   |

   @StockQuotationsProvider_Formatting_4
   Scenario: Format error
      Given the "BTC_EUR" stock quotation table available on the platform is
         | time         | close           |
         | 1586531940   | null            |
      When I get stock quotations from the platform
      Then a warning file is created
      And the "BTC_EUR" currency is marked as "NON_TRADABLE"

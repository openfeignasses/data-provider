@StockQuotationsProvider
@StockQuotationsProvider_Duplicates
@PlatformIndependent
@Stub
Feature: Delete the duplicated data in case of a Platform bug.

   As Bernard the Data provider
   In order to provide consistent data to the other modules
   I want to delete duplicated data in the stock quotations
   This should be done for all available currencies pairs.
   
   Background:
      Given the frequency is "4" hours
      And the period is "3" days
      And the first date is "01/01/2020 00:00"
      And the last date is "03/01/2020 20:00"
      And the current date is "03/01/2020 20:00"
      And the "BTC_EUR" currency is marked as "TRADABLE"

   @StockQuotationsProvider_Duplicates_1
   Scenario: Two lines have the same date and same value
      Given the "BTC_EUR" stock quotation table available on the platform is
         | time         | close           |
         | 1586531940   | 6636.3          |
         | 1586531940   | 6636.3          |
         | 1586539140   | 6636.3          |
      When I get stock quotations from the platform
      Then we obtain this values table for "BTC_EUR"
         | time         | close           |
         | 1586531940   | 6636.3          |
         | 1586539140   | 6636.3          |

   @StockQuotationsProvider_Duplicates_2
   Scenario: Two lines have the same date and same value, multiple times
      Given the "BTC_EUR" stock quotation table available on the platform is
         | time         | close           |
         | 1586531940   | 6636.3          |
         | 1586531940   | 6636.3          |
         | 1586539140   | 6636.3          |
         | 1586539140   | 6636.3          |
      When I get stock quotations from the platform
      Then we obtain this values table for "BTC_EUR"
         | time         | close           |
         | 1586531940   | 6636.3          |
         | 1586539140   | 6636.3          |

   @StockQuotationsProvider_Duplicates_3
   Scenario: Two lines have the same date, different value
      Given the "BTC_EUR" stock quotation with duplicates available on the platform is
      """
      +------------------+
      |                  |
      |    X           XX|
      |   X X        XX X|
      |  X   X      X    |
      | X     XXXXXX     |
      |X                 |
      +------------------+
                        ^
                        |
                       Duplicated
      """
      When I get stock quotations from the platform
      Then a warning file is created
      And the "BTC_EUR" currency is marked as "NON_TRADABLE"

   @StockQuotationsProvider_Duplicates_4
   Scenario: Three lines have the same date, different value
      Given the "BTC_EUR" stock quotation with duplicates available on the platform is
      """
      +------------------+
      |                  |
      |    X           XX|
      |   X X        XX X|
      |  X   X      X   X|
      | X     XXXXXX     |
      |X                 |
      +------------------+
                        ^
                        |
                       Duplicated
      """
      When I get stock quotations from the platform
      Then a warning file is created
      And the "BTC_EUR" currency is marked as "NON_TRADABLE"

   @StockQuotationsProvider_Duplicates_5
   Scenario: Lines have the same date, different value, multiple times
      Given the "BTC_EUR" stock quotation with duplicates available on the platform is
      """
      +------------------+
      |                  |
      |    X           XX|
      |   XXX        XX X|
      |  X   X   X  X    |
      | X     XXXXXX     |
      |X                 |
      +------------------+
           ^     ^      ^
           |     |      |
             Duplicated
      """
      When I get stock quotations from the platform
      Then a warning file is created
      And the "BTC_EUR" currency is marked as "NON_TRADABLE"
@StockQuotationsProvider
@StockQuotationsProvider_Frequency
@PlatformIndependent
@Stub
Feature: Be able to set the frequency as parameter

   As Bernard the Data provider
   To be able to change the frequency of stock quotations when I need, I
   want it to be configurable.
   This should be the case for all available currencies pairs.

   Background:
      Given the frequency is "1" hour for the following table
      And the "BTC_EUR" currency is marked as "TRADABLE"
      And the "BTC_EUR" stock quotation table available on the platform is
         | time         | close           |
         | 1586531940   | 6636.3          |
         | 1586535540   | 6636.3          |
         | 1586539140   | 6636.3          |
      And the frequency is "4" hours for the following table
      And the "BTC_EUR" stock quotation table available on the platform is
         | time         | close           |
         | 1586531940   | 6636.3          |
         | 1586546340   | 6636.3          |
         | 1586560740   | 6636.3          |
      And the current date is "10/04/2020 17:20"

   @StockQuotationsProvider_Frequency_1
   Scenario: Frequency 4 hours
      Given the frequency is "4" hours
      When I get stock quotations from the platform
      Then we obtain this values table for "BTC_EUR"
         | time         | close           |
         | 1586531940   | 6636.3          |
         | 1586546340   | 6636.3          |
         | 1586560740   | 6636.3          |

   @StockQuotationsProvider_Frequency_2
   Scenario: Frequency 1 hour
      Given the frequency is "1" hours
      When I get stock quotations from the platform
      Then we obtain this values table for "BTC_EUR"
         | time         | close           |
         | 1586531940   | 6636.3          |
         | 1586535540   | 6636.3          |
         | 1586539140   | 6636.3          |

   @StockQuotationsProvider_Frequency_3
   Scenario: Frequency not set
      Given the frequency is "-1" hours
      When I try to get stock quotations from the platform
      Then an error is thrown

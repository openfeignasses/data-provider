@StockQuotationsProvider
@StockQuotationsProvider_Incrementation
@PlatformIndependent
@Stub
Feature: Be able to save historical data incrementally

   As Bernard the Data provider
   In order to provide the maximum of historical data to the other modules
   I want to keep old values and save the new ones in the database.

   Note: if a currency is marked as non-tradable for more than the platform
   time-range (720*frequency), then there will be missing values when the currency
   is marked as tradable again. Gaps will have to be filled manually.

   @StockQuotationsProvider_Incrementation_1
   Scenario: Historical data incremented
      Given the "BTC_EUR" currency is marked as "TRADABLE"
      And the frequency is "4" hours
      And the period is "3" days
      And the first date is "01/01/2020 00:00"
      And the last date is "03/01/2020 20:00"
      And the current date is "03/01/2020 20:01"
      And the "BTC_EUR" stock quotation already saved into the database is
      """
      +------------------+
      |                  |
      |     X           X|
      |    X X        XX |
      |   X   X      X   |
      |  X     XXXXXX    |
      |XX                |
      +------------------+
      """
      And the "BTC_EUR" stock quotation available on the platform is
      """
      +-------------------+
      |                  X|
      |                   |
      |                   |
      |                   |
      |                   |
      |                   |
      +-------------------+
      """
      When I get stock quotations from the platform
      Then we obtain these values for "BTC_EUR"
      """
      +-------------------+
      |                  X|
      |     X           X |
      |    X X        XX  |
      |   X   X      X    |
      |  X     XXXXXX     |
      |XX                 |
      +-------------------+
      """
      And the number of returned values is 19

   @StockQuotationsProvider_Incrementation_2
   Scenario: Historical data already up-to-date
      Given the "BTC_EUR" currency is marked as "TRADABLE"
      And the frequency is "4" hours
      And the period is "3" days
      And the first date is "01/01/2020 00:00"
      And the last date is "03/01/2020 20:00"
      And the current date is "03/01/2020 20:01"
      And the "BTC_EUR" stock quotation already saved into the database is
      """
      +-------------------+
      |                  X|
      |     X           X |
      |    X X        XX  |
      |   X   X      X    |
      |  X     XXXXXX     |
      |XX                 |
      +-------------------+
      """
      And the "BTC_EUR" stock quotation available on the platform is
      """
      +-------------------+
      |                  X|
      |                   |
      |                   |
      |                   |
      |                   |
      |                   |
      +-------------------+
      """
      When I get stock quotations from the platform
      Then we obtain these values for "BTC_EUR"
      """
      +-------------------+
      |                  X|
      |     X           X |
      |    X X        XX  |
      |   X   X      X    |
      |  X     XXXXXX     |
      |XX                 |
      +-------------------+
      """
      And the number of returned values is 19

   @StockQuotationsProvider_Incrementation_3
   Scenario: Historical data incremented
      Given the "BTC_EUR" currency is marked as "TRADABLE"
      And the frequency is "4" hours
      And the period is "3" days
      And the first date is "01/01/2020 00:00"
      And the last date is "03/01/2020 20:00"
      And the current date is "03/01/2020 20:01"
      And the "BTC_EUR" stock quotation already saved into the database is
      """
      +-------------------+
      |                  X|
      |     X           X |
      |    X X        XX  |
      |   X   X      X    |
      |  X     XXXXXX     |
      |XX                 |
      +-------------------+
      """
      And the "BTC_EUR" stock quotation available on the platform is
      """
      +---------------------+
      |                   XX|
      |                     |
      |                     |
      |                     |
      |                     |
      |                     |
      +---------------------+
      """
      When I get stock quotations from the platform
      Then we obtain these values for "BTC_EUR"
      """
      +---------------------+
      |                  XXX|
      |     X           X   |
      |    X X        XX    |
      |   X   X      X      |
      |  X     XXXXXX       |
      |XX                   |
      +---------------------+
      """
      And the number of returned values is 21

   @StockQuotationsProvider_Incrementation_4
   Scenario: Historical data incremented, also when previously marked non-tradable
      Given the "BTC_EUR" currency is marked as "NON_TRADABLE"
      And the frequency is "4" hours
      And the period is "3" days
      And the first date is "01/01/2020 00:00"
      And the last date is "03/01/2020 20:00"
      And the current date is "03/01/2020 20:01"
      And the "BTC_EUR" stock quotation already saved into the database is
      """
      +-------------------+
      |                  X|
      |     X           X |
      |    X X        XX  |
      |   X   X      X    |
      |  X     XXXXXX     |
      |XX                 |
      +-------------------+
      """
      And the "BTC_EUR" stock quotation available on the platform is
      """
      +---------------------+
      |                   XX|
      |                     |
      |                     |
      |                     |
      |                     |
      |                     |
      +---------------------+
      """
      When I get stock quotations from the platform
      Then we obtain these values for "BTC_EUR"
      """
      +---------------------+
      |                  XXX|
      |     X           X   |
      |    X X        XX    |
      |   X   X      X      |
      |  X     XXXXXX       |
      |XX                   |
      +---------------------+
      """
      And the number of returned values is 21

@StockQuotationsProvider
@StockQuotationsProvider_ServerAndNetworkErrors
@PlatformIndependent
@Stub
Feature: Get stock quotations for a period relative to the current date

   As Bernard the Data provider
   In order to provide stock quotations to the other modules
   I want to properly communicate with the configured platform.

   Background:
      Given the frequency is "4" hours
      And the "BTC_EUR" currency is marked as "TRADABLE"
      And the current date is "10/04/2020 17:20"

   @StockQuotationsProvider_Get_1
   Scenario: Server error
      Given the error field in the server response contains one element for "BTC_EUR"
      And it is not a warning
      When I try to get stock quotations from the platform
      Then a warning file is created
      And the "BTC_EUR" currency is flagged as "NON_TRADABLE"

   @StockQuotationsProvider_Get_2
   Scenario: Server warning
      Given the error field in the server response contains one element for "BTC_EUR"
      And it is a warning
      When I try to get stock quotations from the platform
      Then a warning file is created
      And the "BTC_EUR" currency is flagged as "NON_TRADABLE"

   @StockQuotationsProvider_Get_3
   Scenario: Communication error
      Given the server is not returning a response while requesting "BTC_EUR"
      And the request timeout is 10 seconds
      When I try to get stock quotations from the platform
      Then a warning file is created
      And the "BTC_EUR" currency is flagged as "NON_TRADABLE"
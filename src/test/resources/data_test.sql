-- Always mark pairs used in behaviour tests as force ignore by default
-- This way, they will be ready to be "added" to the investment universe in scenario
-- by marking them as "TRADABLE" or "NON_TRADABLE".
-- A currency that is marked as force ignore will still have this status until a human decides to change it.
INSERT INTO `cryptobot`.`investment_universe_status` (`name`, `status`) VALUES ('BTC_EUR', 'FORCE_IGNORE');
INSERT INTO `cryptobot`.`investment_universe_status` (`name`, `status`) VALUES ('ETH_EUR', 'FORCE_IGNORE');